package es.unex.asee.frojomar.asee_ses.listcenters.unittest;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;
import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.core.app.ApplicationProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import java.io.IOException;
import java.util.List;

import es.unex.asee.frojomar.asee_ses.model.Center;
import es.unex.asee.frojomar.asee_ses.repository.CentersData;
import es.unex.asee.frojomar.asee_ses.repository.room.roomdb.AppDatabase;
import es.unex.asee.frojomar.asee_ses.repository.room.roomdb.CentersDAO;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;

public class CentersDataUnitTest {

    // necessary to test the LiveData
    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    // create a variable to retrieve the CentersData instance
    //NOTE: CentersData encloses the origin of the data. We do not know if it is local or API REST data.
    private CentersData centersData;

    @Before
    public void getInstance() {
        //get an instance of the CentersData class
        centersData = CentersData.getInstance();
    }


    @Test
    public void writeCenterAndReadInDataBase() throws Exception {

        //we recover the centers (in a LiveData)
        LiveData<List<Center>> livecenters = centersData.getAllCenters(getInstrumentation().getTargetContext());
        //we retrieve LiveData information with getValue() from LiveDataTestUtils
        List<Center> centers = LiveDataTestUtils.getValue(livecenters);
        //check that centers is not null
        assertTrue(centers != null);
        /*we can not to check data, because we don't know their origin.
        We can not mock internal classes of CentersData in this test, without add dependency
            of other class (which must be tested in tests intended for themselves).*/
    }

    //auxiliar method to create more easily centers to the tests
    public static Center createCenter(Integer id){
        Center c = new Center();
        c.setId(id);
        c.setAddress("address");
        c.setName("name");
        c.setLat(-6.13);
        c.setLong(39.31);
        return c;
    }

}